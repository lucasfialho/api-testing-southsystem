# API-Testing-SouthSystem

This project is an automated test suite for the PokéAPI, programmed in *Java* and using **Maven** as project software management. After running the test suite, an **Allure** report is generated with the results in a `HTML` file.
**Frameworkium** was used to take care of structure and dependency matters, giving a strong code structure avoiding the developer to be reinventing the wheel.

## Index

1. #### [Used Tools](#used-tools-1)
1. #### [Documentation](#documentation-1)
1. #### [Configuration](#configuration-1)
1. #### [How To Run](#how-to-run-1)


## Used Tools

#### PokeAPI
It is an public API which provides highly detailed
data related to Pokémon. The goal of this website
is to provide a single source of Pokémon data.

https://pokeapi.co/

#### Frameworkium (Maven, Allure and TestNG)

It is a open-source Java framework which provides a strong code 
structure and has Java Libraries that makes writing
automated tests more consistent and well structured.
Its main focus is Web UI Automation, but it can also
be used for mobile app, API and etc.

Frameworkium uses Maven as project software management
with TestNG as a testing framework and Allure as 
testing report. The framework comes versions and
dependencies already matching, making the set up 
easier.

https://frameworkium.github.io/

#### Allure Report

It's a open-source framework designed to create a detailed test report with a good representation of the tests. This framework is already included by [Frameworkium](#frameworkium-maven-allure-and-testng) and have compatibility with **TestNG** and **Maven**.

http://allure.qatools.ru/

## Documentation

The solution is divided in 4 folders (constant, dto, service and tests).

* **Constant**

This folder contains the endpoints of the API, it was created only one class containing the different "sections" of the PokéAPI. This class is responsible to retrieve the endpoints for the service structure to be consumed depending of the information needed. The endpoints mapped from the PokéAPI were the *base_uri*, *pokemon_list*, *pokemon* (details of a given pokémon), *moves*, *ability_list* and *ability*.

* **DTO**

Here contains all the mapped structures from the endpoints used, it helps deserializing the JSON information from the API and also checks if the structure was modified. If a field was included and not mapped in the automation, it will throw an exception. The DTO can not only be used to access the received data from the API but it can also be used to a `POST` action without needing to create a JSON.

* **Service**

In the Service is all the logic behind the tests, there it implements the actions made to the API Service. When searching for a Pokémon in the API, it will be in the Service class where it will connect to the Endpoint requesting the information from a given Pokémon. These fuction always return some data from the API that will be used to pass or fail the tests.

* **Tests**

All the tests definition can be found here, this class connects with the Service asking for one of the mapped actions. With the data retrieved from the Service, it will make asserts that verify if the content is valid and correct. The test asserts are stricted to this class. A DataProvider was used to populate data to the tests, the data was described in the class since it was not a large quantity of data, but it also could be used an `.csv` file, database and etc. store it.

## Configuration
With the [Frameworkium](#frameworkium) as framework, a lot of the configurations are already set up. But it is still needed to have [Java JDK 11](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html) and [Maven](https://maven.apache.org/download.cgi) installed to run without complications. 

Enviroment Variables also need to be set up to run the tests using Maven. The instructions to install Maven and run the tests can be found [here](https://maven.apache.org/install.html)

(*Always remember to restart the application, terminal or the machine after configuring the environment variables*).


## How To Run

After having the **Java** and **Maven** installed, the tests should run successfully. Open a terminal in the folder where contains the `pom.xml` file, in this case, `api-testing-southsystem` folder (if you opened the project in a IDE, its terminal also should work).

1. ###### Run Automated Tests
    To run the test use the following command:
    ```unix
    mvn clean verify
    ```
    This command will run all the test suite, but if you want to run a specific test use this command:
    ```unix
    mvn clean verify -Dtest=<TestClassName>#<MethodName>
    ```
    If you want to run the Count Pokemon test for instance, the command would be:
    ```unix
    mvn clean verify -Dtest=PokemonTest#CountPokemon
    ```

1. ###### Generate Allure Test Report
    After running the automated tests, you can generate a `HTML` test report running the following command:
    ```unix
    mvn allure:report
    ```

    After the command is over it will mentioned the folder's default location where it can be found.

    

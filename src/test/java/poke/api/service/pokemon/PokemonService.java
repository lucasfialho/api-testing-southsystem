package poke.api.service.pokemon;


import poke.api.constant.PokeEndpoint;
import poke.api.dto.moves.Move;
import poke.api.dto.abilities.Ability;
import poke.api.dto.pokemon.*;
import poke.api.service.AbstractPokemonService;

import java.util.List;

public class PokemonService extends AbstractPokemonService {

    public List<PokemonList> listPokemons() {
        return get(PokeEndpoint.POKEMON_LIST.getUrl())
                .jsonPath().getList("results", PokemonList.class);
    }

    public int countPokemon(){
        return get(PokeEndpoint.POKEMON_LIST.getUrl()).jsonPath().get("count");
    }

    public Pokemon searchPokemon(String name){
        return get(PokeEndpoint.POKEMON.getUrl(name)).as(Pokemon.class);
    }

    public Move searchMove(String name){
        return get(PokeEndpoint.MOVES.getUrl(name)).as(Move.class);
    }

    public Ability searchAbility (String name){
        return get(PokeEndpoint.ABILITY.getUrl(name)).as(Ability.class);
    }

    public Ability createAbility(Ability newAbility){
        return post(newAbility, PokeEndpoint.ABILITY_LIST.getUrl()).as(Ability.class);
    }

    public Object createInvalidPost(Object invalidData, String endpoint){
        return post(invalidData, endpoint);
    }

    public Pokemon accessPokemonFromSearch(PokemonList pokemon){
        return get(pokemon.url).as(Pokemon.class);
    }

}

package poke.api.constant;

import com.frameworkium.core.api.Endpoint;


public enum PokeEndpoint implements Endpoint {

    BASE_URI("https://pokeapi.co/api/v2"),
    POKEMON_LIST("/pokemon?offset=0&limit=2000"),
    POKEMON("/pokemon/%s"),
    MOVES("/move/%s"),
    ABILITY_LIST("/ability"),
    ABILITY("/ability/%s");

    private String url;

    PokeEndpoint(String url) {
        this.url = url;
    }

    /**
     * @param params Arguments referenced by the format specifiers in the url.
     * @return A formatted String representing the URL of the given constant.
     */
    @Override
    public String getUrl(Object... params) {
        return String.format(url, params);
    }

}

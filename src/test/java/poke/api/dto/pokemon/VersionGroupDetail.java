
package poke.api.dto.pokemon;


public class VersionGroupDetail {

    public Integer levelLearnedAt;
    public MoveLearnMethod moveLearnMethod;
    public VersionGroup versionGroup;

}

package poke.api.dto.pokemon;

import com.frameworkium.core.api.dto.AbstractDTO;

public class PokemonList extends AbstractDTO<PokemonList> {

    public String name;
    public String url;

}


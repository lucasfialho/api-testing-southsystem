
package poke.api.dto.pokemon;

import java.util.List;

public class Pokemon {

    public List<Ability> abilities = null;
    public Integer base_experience;
    public List<Form> forms = null;
    public List<Game_index> game_indices = null;
    public Integer height;
    public List<Held_item> held_items = null;
    public Integer id;
    public Boolean is_default;
    public String location_area_encounters;
    public List<Move> moves = null;
    public String name;
    public Integer order;
    public Species species;
    public Sprites sprites;
    public List<Stat> stats = null;
    public List<Type> types = null;
    public Integer weight;

}

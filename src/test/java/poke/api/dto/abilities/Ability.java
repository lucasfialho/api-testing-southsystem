
package poke.api.dto.abilities;

import restfulbooker.api.dto.booking.Booking;
import restfulbooker.api.dto.booking.BookingDates;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.ThreadLocalRandom;

public class Ability {

    public List<Object> effect_changes = null;
    public List<Effect_entry> effect_entries = null;
    public List<Flavor_text_entry> flavor_text_entries = null;
    public Generation generation;
    public Integer id;
    public Boolean is_main_series;
    public String name;
    public List<Name> names = null;
    public List<Pokemon> pokemon = null;

    public static Ability newInstance() {
        var random = ThreadLocalRandom.current();
        int randInt = random.nextInt();
        var ability = new Ability();

        ability.id = randInt;
        ability.is_main_series = false;
        ability.name = "test";

        return ability;
    }
}


package poke.api.dto.moves;

import java.util.List;

public class Move {

    public Integer accuracy;
    public Contest_combos contest_combos;
    public Contest_effect contest_effect;
    public Contest_type contest_type;
    public Damage_class damage_class;
    public Object effect_chance;
    public List<Object> effect_changes = null;
    public List<Effect_entry> effect_entries = null;
    public List<Flavor_text_entry> flavor_text_entries = null;
    public Generation generation;
    public Integer id;
    public List<Object> machines = null;
    public Meta meta;
    public String name;
    public List<Name> names = null;
    public List<Object> past_values = null;
    public Integer power;
    public Integer pp;
    public Integer priority;
    public List<Object> stat_changes = null;
    public Super_contest_effect super_contest_effect;
    public Target target;
    public Type type;

}

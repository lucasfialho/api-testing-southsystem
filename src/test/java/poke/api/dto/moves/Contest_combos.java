
package poke.api.dto.moves;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "normal",
        "super"
})

public class Contest_combos {

    @JsonProperty("normal")
    public Normal normal;
    @JsonProperty("super")
    public Super _super;


}

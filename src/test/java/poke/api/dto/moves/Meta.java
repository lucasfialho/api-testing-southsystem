
package poke.api.dto.moves;


public class Meta {

    public Ailment ailment;
    public Integer ailment_chance;
    public Category category;
    public Integer crit_rate;
    public Integer drain;
    public Integer flinch_chance;
    public Integer healing;
    public Object max_hits;
    public Object max_turns;
    public Object min_hits;
    public Object min_turns;
    public Integer stat_chance;

}

package poke.api.tests;

import com.frameworkium.core.api.tests.BaseAPITest;
import com.frameworkium.core.common.retry.RetryFlakyTest;
import io.qameta.allure.Description;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import poke.api.constant.PokeEndpoint;
import poke.api.dto.abilities.Ability;
import poke.api.dto.pokemon.Pokemon;
import poke.api.service.pokemon.PokemonService;

import static com.google.common.truth.Truth.assertThat;

@Test(retryAnalyzer = RetryFlakyTest.class)
public class PokemonTest extends BaseAPITest {

    @DataProvider(name = "pokemon")
    public static Object[] pokemonData() {

        return new Object[] { "bulbasaur", "charmander", "trevenant", "squirtle", "pikachu", "celebi", "eevee", "ditto",
                "oddish", "gloom", "vileplume", "snorlax", "mew"};

    }

    @DataProvider(name = "moves")
    public static Object[] movesData() {

        return new Object[] { "absorb", "seismic-toss", "strength", "leech-seed", "growth", "poison-powder", "stun-spore", "mimic",
                "minimize", "harden", "swift", "bubble", "conversion"};
    }

    @DataProvider(name = "abilities")
    public static Object[] abilitiesData(){
        return new Object[]{"mold-breaker", "magic-guard", "flame-body", "wonder-guard","serene-grace",
                "cute-charm", "weak-armor", "unburden", "no-guard", "shadow-tag"};
    }

    @DataProvider(name = "endpoints")
    public static Object[] endpointsData(){
        return new Object[]{PokeEndpoint.POKEMON_LIST.getUrl(), PokeEndpoint.ABILITY_LIST.getUrl(),
                PokeEndpoint.BASE_URI.getUrl()};
    }

    @Description("Check if the number of pokemon entries are the same as the field 'count' in the API")
    public void countPokemon() {
        var service = new PokemonService();
        var pokemonID = service.listPokemons();

        var countPokeApi = service.countPokemon();

        assertThat(pokemonID.size()).isEqualTo(countPokeApi);

    }
    @Description("Check if a valid pokemon can be found and accessed and if the pokemon data structure is the same")
    @Test(dataProvider = "pokemon")
    public void searchPokemon(String name){
        var service = new PokemonService();
        var pokemon = service.searchPokemon(name);

        assertThat(pokemon.name).isEqualTo(name);
    }

    @Description("Check if a valid move can be found and accessed and if the move data structure is the same")
    @Test(dataProvider = "moves")
    public void searchMove(String moveData){
        var service = new PokemonService();
        var move = service.searchMove(moveData);

        assertThat(move.name).isEqualTo(moveData);
    }

    @Description("Check if a valid ability can be found and accessed and if the ability data structure is the same")
    @Test(dataProvider = "abilities")
    public void searchAbility(String abilityData){
        var service = new PokemonService();
        var ability = service.searchAbility(abilityData);

        assertThat(ability.name).isEqualTo(abilityData);
    }

    @Description("Check if a the endpoint is still case sensitive and the information is not accessible (Negative Scenario)")
    @Test(dataProvider = "abilities")
    public void SearchAbilityWithUpperCase(String abilityData){
        var service = new PokemonService();
        Ability ability = null;
        try {
            ability = service.searchAbility(abilityData.toUpperCase());

        }

        catch (java.lang.AssertionError e){
            assertThat(e.getMessage()).contains("404");
            return;
        }

        assertThat(true).isEqualTo(false);
    }

    @Description("Try to create a new ability in the endpoint (Negative Scenario)")
    public void TryToCreateAbility(){
        var service = new PokemonService();
        var newAbility = Ability.newInstance();
        try {
            Ability ability = service.createAbility(newAbility);
        }

        catch(java.lang.AssertionError e){
            assertThat(e.getMessage()).contains("404");
            return;

        }

        assertThat(true).isEqualTo(false);
    }
    @Description("Check if the Bad Request code is received after sending an invalid request")
    @Test(dataProvider = "endpoints")
    public void CreateInvalidPost(String endpointsData){
        var service = new PokemonService();
        try {
            service.createInvalidPost("@@@@", endpointsData);
        }

        catch(java.lang.AssertionError e){
            assertThat(e.getMessage()).contains("400");
            return;

        }

        assertThat(true).isEqualTo(false);
    }

    @Description("Check if the link between the Pokemon List and the Pokemon Structure is accessible and refer to the same pokemon")
    public void AccessPokemonBySearch(){
        var service = new PokemonService();
        var pokemonList = service.listPokemons();

        for (var i = 0; i<10; i++) {
            var pokemonSearched = pokemonList.get(i);
            var pokemon = service.accessPokemonFromSearch(pokemonSearched);
            assertThat(pokemon.name).isEqualTo(pokemonSearched.name);
            //Since the Pokemon list is ordered by the id, it can be checked by the List index+1
            assertThat(pokemon.id).isEqualTo(i+1);
        }

    }



}
